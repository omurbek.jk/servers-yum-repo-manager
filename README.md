servers-yum-repo-manager
============================



## Description
This composite creates a simple YUM repository manager instance via an Autoscaling Group


## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/servers-yum-repo-manager/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

### `PRIVATE_SUBNET_NAME`:
  * description: 

### `VPC_NAME`:
  * description: 


## Required variables with default

### `VPC_CIDRS`:
  * description: 
  * default: [10.0.0.0/16]

### `YUM_MANAGER_AMI`:
  * description: the ami for the yum repo manager
  * default: ami-76817c1e


### `YUM_MANAGER_INGRESS_CIDRS`:
  * description: the ip addresses allowed to access the yum repo manager
  * default: 0.0.0.0/0

### `YUM_MANAGER_INGRESS_PORTS`:
  * description: ports that should be open to the world
  * default: 1388, 16

### `YUM_MANAGER_KEYPAIR`:
  * description: 
  * default: cloudcoreo-dev


### `YUM_MANAGER_NAME`:
  * description: 
  * default: yum-repo-manager


### `YUM_MANAGER_SIZE`:
  * description: 
  * default: m3.medium


### `YUM_REPO_BUCKET`:
  * description: the actual bucket that should contain the yum repository
  * default: jarvis-test


### `YUM_REPO_BUCKET_REGION`:
  * description: the actual bucket that should contain the yum repository
  * default: us-east-1



## Optional variables with default

**None**


## Optional variables with no default

### `SUMOLOGIC_KEY`:
  * description: Sumologic api key - if left blank, sumo will not be installed

### `SUMOLOGIC_ID`:
  * description: Sumologic api id - if left blank, sumo will not be installed

## Tags
1. Servers
1. YUM


## Categories
1. Servers



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/servers-yum-repo-manager/master/images/diagram.png "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/servers-yum-repo-manager/master/images/icon.png "icon")

