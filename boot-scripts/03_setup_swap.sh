#!/bin/bash

# Give a little more ram for the createrepo command to do it's thing...
test -e /media/ephemeral0/swap.1 || {
    /bin/dd if=/dev/zero of=/media/ephemeral0/swap.1 bs=1M count=3024;
    /sbin/mkswap /media/ephemeral0/swap.1;
    /sbin/swapon /media/ephemeral0/swap.1;
    echo "/media/ephemeral0/swap.1 swap swap defaults 0 0" >> /etc/fstab
}

