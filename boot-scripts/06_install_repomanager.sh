#!/bin/bash

yum install -y yum-repo-manager

# Perform the initial sync to our local repo staging area
mkdir -p /tmp/repostaging/repo
mkdir -p /media/ephemeral0/repocache
aws s3 sync s3://${YUM_REPO_BUCKET}/repo /tmp/repostaging/repo --region ${YUM_REPO_BUCKET_REGION}
chown -R ec2-user: /tmp/repostaging

mkdir -p /media/ephemeral0/repocache
chown -R ec2-user: /media/ephemeral0/repocache

# Run our management script every minute.
# Note that this statement will overwrite any other cron entries that already existed.  The python
# script will stay running once started (running a Flask web server), so the cron is just a poor man's
# means of ensuring that it always stays running

dd_string=""
if [ -n "${DATADOGAPIKEY:-}" ]; then
    dd_string="--datadog-api-key ${DATADOGAPIKEY} --datadog-app-key ${DATADOGAPPKEY}"
fi

echo "* * * * * /opt/yum-repo-manager.py --bucket-name ${YUM_REPO_BUCKET} ${dd_string}" | crontab

