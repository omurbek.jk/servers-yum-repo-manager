#!/bin/bash

# Prep the local staging area and logging dirs
mkdir -p /tmp/repostaging
chown -R ec2-user:ec2-user /tmp/repostaging

mkdir -p /var/log/yum-repo-manager
chmod -R 0755 /var/log/yum-repo-manager
chown -R ec2-user:ec2-user /var/log/yum-repo-manager
