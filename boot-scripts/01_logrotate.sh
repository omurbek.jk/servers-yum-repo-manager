#!/bin/bash

# Configure logrotate to rotate the manager logs daily
cat >> /etc/logrotate.d/yum-repo-manager <<EOF
/var/log/yum-repo-manager/manager.log {
    daily
    rotate 7
    compress
}
EOF
