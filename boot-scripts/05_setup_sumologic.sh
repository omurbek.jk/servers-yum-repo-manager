#!/bin/bash

if [ -z "${SUMOLOGIC_ID}" ]; then
    exit 0;
fi
if [ -z "${SUMOLOGIC_KEY}" ]; then
    exit 0;
fi
HOSTNAME="$(hostname)"
(
    cd /tmp/
    wget -O sumo.sh https://collectors.sumologic.com/rest/download/linux/64
    chmod +x sumo.sh
    
    mkdir -p /etc/sumo/
    cat <<EOF > /etc/sumo.conf
accessid=${SUMOLOGIC_ID}
accesskey=${SUMOLOGIC_KEY}
sources=/etc/sumo/sumo.json
ephemeral=true
EOF
    cat <<EOF > /etc/sumo/sumo.json
{
  "api.version": "v1",
  "sources": [
    {
      "name" : "CloudCoreo",
      "category": "installer",
      "pathExpression" : "/var/log/cloudcoreo-client.log",
      "sourceType" : "LocalFile",
      "automaticDateParsing": true,
      "multilineProcessingEnabled": true,
      "useAutolineMatching": true,
      "forceTimeZone": false,
      "timeZone": "${TIMEZONE}",
      "filters": [],
      "blacklist": []
    }
    ,{
      "name" : "yum-repo-manager",
      "category": "tools",
      "pathExpression" : "/var/log/yum-repo-manager/*.log",
      "sourceType" : "LocalFile",
      "automaticDateParsing": true,
      "multilineProcessingEnabled": true,
      "useAutolineMatching": true,
      "forceTimeZone": false,
      "timeZone": "${TIMEZONE}",
      "filters": [],
      "blacklist": []
    }
  ]
}
EOF

    ./sumo.sh -q
)

