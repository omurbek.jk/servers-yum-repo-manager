#!/bin/bash

# Install some basic packages required to operate a yum repo (and even some stuff to build RPMs)
yum -y install rpm rpmdevtools tree git createrepo python-setuptools
easy_install pip
pip install boto
pip install requests
pip install flask
pip install apscheduler==2.1.0
pip install --allow-all-external dogapi
pip install dogapi
pip install awscli
